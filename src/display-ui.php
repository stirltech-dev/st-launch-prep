<?php

namespace ST\LaunchPrep\DisplayUI;

use ST\LaunchPrep\Utils;

function setup() {
    add_action(
        'plugins_loaded', function() {
            add_action( 'wp_enqueue_scripts', __NAMESPACE__ . '\enqueue_scripts_styles' );
            add_action( 'wp_footer', __NAMESPACE__ . '\add_modal' );
        }
    );
}

function enqueue_scripts_styles() {
    if (Utils\is_complete()) {
       return;
    }   
    wp_enqueue_script( 'stlp-fb', Utils\asset_path() . '/js/jquery.fancybox.min.js', array('jquery'), false, true );
    wp_enqueue_script( 'stlp-display', Utils\asset_path() . '/js/display.js', array('jquery'), false, true );
    wp_enqueue_style( 'stlp-fb',  Utils\asset_path() . '/css/jquery.fancybox.min.css' );
    wp_enqueue_style( 'stlp-display',  Utils\asset_path() . '/css/display.css' );
}

function add_modal() {
    if (Utils\is_complete()) {
        return;
    }
    include_once( __DIR__ . '/views/modal.php');
}

