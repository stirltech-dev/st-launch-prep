<?php

namespace ST\LaunchPrep;

class Get_valid_data {
    private $provider;
    private $email;
    private $username ;
    private $error_array = array();

    function __construct($provider,  $email, $username)
    {
        $this->provider = $provider;
        $this->email = $email;
        $this->username = $provider;
    }

    // Provider
    public function valid_provider()
    {
        if ( !preg_match("/^[A-Za-z][A-Za-z0-9]{3,31}$/", $this->provider)){
            array_push( $this->$error_array,  new WP_Error( 'rest_invalid_provider', esc_html__( 'The Invalid Provider Name.'))); 
            return false;
        }
        return true;
    }

    // Email 
    public function valid_email()
    {
        if (!preg_match("/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/", $this->email)) {
            array_push( $error_array, new WP_Error( 'rest_invalid_email', esc_html__( 'The Invalid Email Address.'))); 
            return false;
         }
        return true;
    }

    // Username
    public function valid_username()
    {
        if ( !preg_match("/^[A-Za-z][A-Za-z0-9]{3,31}$/", $this->username)){
            array_push( $this->$error_array, new WP_Error( 'rest_invalid_username', esc_html__( 'The Invalid username.'))); 
            return false;
        }
        return true;
    }
    
    // The main validation functiom 
    public function data_validation()
    {
       if( $this->valid_provider() && $this->valid_email() && $this->valid_username()){
           return true;
       }
       return false;
    }

    // Display Error if needed 
    public function get_error(){
        foreach ($error_array as $error){
            print_r($error. '\n');
        }
    }
}
