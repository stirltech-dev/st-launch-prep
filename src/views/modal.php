<?php ?>
<div id="st-form-wrapper">
    <form method="post" id="st-form">
        <div class="provider-section">
            <label for="stlp-provider">Provider</label>
            <select name="provider" id="stlp-provider" required>
                <option value="GoDaddy">GoDaddy</option>
                <option value="Network Solutions">Network Solutions</option>
                <option value="Register.com">Register.com</option>
                <option value="Other">Other</option>
            </select>
        </div>
        <div class="login_uri-section">
            <label for="stlp-login_uri">Login URI</label>
            <input type="text" id="stlp-login_uri" name="login_uri" required />
            <p class="stlp-error">Invalid uri!</p>
        </div>
        <div class="username-section">
            <label for="stlp-username">Username, Email, or Customer ID*</label>
            <input type="text" id="stlp-username" name="username" required />
            <p class="stlp-error">Invalid username!</p>
        </div>
        <div class="password-section">
            <label for="stlp-password">Password*</label>
            <input type="password" id="stlp-password" name="password" required />
            <p class="stlp-error">Invalid password!</p>
        </div>
        <input type="submit" id="btn-submit" />
        <span class="stlp-success-message">Thanks!</span>
    </form>
</div>
