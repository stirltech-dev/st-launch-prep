(function($){
    setTimeout(function(){
        $.fancybox.open({
            src: '#st-form-wrapper',
            type: 'inline',
            opts: {
                afterClose: function () {
                    setCookie('stlp', 'true', 2);
                }
            }
        });
    }, 1000);

    $('#st-form').on('submit', function(e){
        var $submit = $('#st-form [type=submit]').attr('disabled', 'disabled');
        $(`#st-form input`).removeClass('error');
        e.preventDefault();
        
        $.post('/wp-json/st/v1/launch-prep', $( this ).serialize())
            .then(function(data) {
                $('.stlp-success-message').show();
                setTimeout(function(){ $.fancybox.close( true ); }, 1100);
            })
            .fail(function(xhr){
                var error = JSON.parse(xhr.responseText);
                if (error.data.status === 400) {
                    $.each(error.data.params, function(key, val) {
                        $(`#st-form input[name=${key}]`).addClass('error');
                    }) ;
                }
            })
            .always(function(){
                $submit.removeAttr('disabled');
            })
    })

    /* ----------- Cookie Function --------------*/
    function setCookie(name,value,days) {
        var expires = "";
        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + (days*24*60*60*1000));
            expires = "; expires=" + date.toUTCString();
        }
        document.cookie = name + "=" + (value || "")  + expires + "; path=/";
    }

})(jQuery)


