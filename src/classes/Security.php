<?php

namespace ST\LaunchPrep;

use ST\LaunchPrep\Utils;
use Defuse\Crypto\Key;
use Defuse\Crypto\Crypto;
use \Defuse\Crypto\Exception\WrongKeyOrModifiedCiphertextException;

// @see https://github.com/defuse/php-encryption/blob/master/docs/Tutorial.md

class Security
{
    /**
     * Load the key from our key file
     *
     * @return void
     */
    public static function key() 
    {
        $keyAscii = file_get_contents(
            Utils\key_path()
        );
        return Key::loadFromAsciiSafeString($keyAscii);
    } 

    /**
     * Encrypt the password
     *
     * @param string $secret The value to be encrypted
     * @return string The encrypted cipher
     */
    public static function encrypt($secret) 
    {
        return Crypto::encrypt($secret, static::key());
    }

    /**
     * Decrypt the password
     *
     * @param string $cipher The cipher stored in the db
     * @return string The unencrypted secret
     */
    public static function decrypt($cipher) 
    {
        return Crypto::decrypt($cipher, static::key());
    }
}