<?php

namespace ST\LaunchPrep\Utils;

use ST\LaunchPrep\Settings;

function is_complete() {
    return Settings::exist();
}

function asset_path() {
    return plugin_base_url() . '/assets';
}

function key_path() {
    return plugin_base_path() . 'key.txt';
}

function plugin_base_url() {
    return plugin_dir_url( dirname( __FILE__ ) );
}

function plugin_base_path() {
    return plugin_dir_path( dirname( __FILE__ ) );
}