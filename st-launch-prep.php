<?php
/**
 * Plugin Name: Launch Prep
 * Plugin URI: https://stboston.com/
 * Description: Gather required information for new WP project launches
 * Version: 0.1
 * Author: Stirling Technologies
*/

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

define('STLP_OPTION_NAME', '_stlp');
define('STLP_COOKIE_NAME', '_stlp');

// Require our crypto library
require_once( __DIR__ . '/vendor/autoload.php' );

/**
 * PSR-4 autoloading
 */
spl_autoload_register(
	function( $class ) {
        // project-specific namespace prefix
        $prefix = 'ST\\LaunchPrep\\';
        // base directory for the namespace prefix
        $base_dir = __DIR__ . '/src/classes/';
        // does the class use the namespace prefix?
        $len = strlen( $prefix );
		if ( strncmp( $prefix, $class, $len ) !== 0 ) {
			return;
		}
        $relative_class = substr( $class, $len );
        
        $file = $base_dir . str_replace( '\\', '/', $relative_class ) . '.php';
        // if the file exists, require it
		if ( file_exists( $file ) ) {
			require $file;
		}
	}
);

require_once __DIR__ . '/src/utils.php';
require_once __DIR__ . '/src/display-ui.php';
require_once __DIR__ . '/src/rest-api.php';
require_once __DIR__ . '/src/settings-ui.php';


\ST\LaunchPrep\DisplayUI\setup();
\ST\LaunchPrep\SettingsUI\setup();
\ST\LaunchPrep\RestAPI\setup();

