<?php

namespace ST\LaunchPrep\SettingsUI;

use ST\LaunchPrep\Settings;

function setup() {
    add_action(
        'plugins_loaded', function() {
            add_action( 'admin_menu', __NAMESPACE__ . '\options_page' );
            add_action( 'admin_init', __NAMESPACE__ . '\register_settings' );
        }
    );
}

function options_page() {
    add_options_page(
        'Domain/DNS Details', 
        'Domain/DNS', 
        'manage_options',
        'stlp',
        __NAMESPACE__ . '\render_options_page'
    );
} 

function render_options_page() {
    ?>
    <div class="wrap">
        <h1><?php echo esc_html( get_admin_page_title() ); ?></h1>
        <?php settings_errors( 'st_messages' ); ?>
        <?php $settings = Settings::get(); ?>
        <table>
            <?php foreach ($settings as $key => $value) : ?>
                <tr>
                    <td><strong><?= $key; ?></strong></td>
                    <td><?= $value; ?></td>
                </tr>
            <?php endforeach; ?>
        </table>
        <!-- <form action="options.php" method="post">
            <?php
                // settings_fields( 'st' );
                // do_settings_sections( 'st' );
                // submit_button( 'Save Settings' );
            ?>
        </form> -->
    </div>
    <?php
}

function register_settings()
{
    // register a new setting for "reading" page
    register_setting('st', 'st_lanch_prep', [
        'sanitize_callback' => __NAMESPACE__ . '\validate_email_field',
    ]);
 
    // register a new section in the "reading" page
    add_settings_section(
        'st_lanch_prep_settings_section',
        'Your site credentials',
        __NAMESPACE__ . '\st_lanch_prep_section_cb',
        'st'
    );
 
    // register a new field in the "wporg_settings_section" section, inside the "reading" page
    add_settings_field(
        'st_lanch_prep',
        'Site credentials',
        __NAMESPACE__ . '\st_launch_prep_field_cb',
        'st',
        'st_lanch_prep_settings_section'
    );
}

function st_lanch_prep_section_cb() {
    echo '';
}

function st_launch_prep_field_cb() {
    
}

