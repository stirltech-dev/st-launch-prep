<?php

namespace ST\LaunchPrep;

use ST\LaunchPrep\Security;

class Settings
{
    /**
     * Option key to save settings
     *
     * @var string
     */
    protected static $option_key = STLP_OPTION_NAME;
    
    /**
     * Default settings
     *
     * @var array
     */
    protected static $defaults = [
        'provider' => '',
        'username' => '',
        'password' => '',
        'login_uri' => ''
    ];
    
    /**
     * Get saved settings
     *
     * @return array
     */
    public static function get()
    {
        $saved = get_option(self::$option_key, array());
        if (empty($saved)) {
            return self::$defaults;
        }
        $decrypt = Security::decrypt($saved);
        return wp_parse_args(
            maybe_unserialize($decrypt),
            self::$defaults
        );
    }

    /**
     * Save settings
     *
     * Array keys must be whitelisted (IE must be keys of self::$defaults
     *
     * @param array $settings An array of settings
     */
    public static function save( array $settings )
    {
        //remove any non-allowed indexes before save
        foreach ( $settings as $i => $setting ){
            if (! array_key_exists($i, self::$defaults) ) {
                unset($settings[ $i ]);
            }
        }
        $settings_secure = Security::encrypt(serialize($settings));
        update_option(self::$option_key, $settings_secure);
    }

    /**
     * Settings exist
     *
     * @param boolean Whether we have the settings
     */

     public static function exist()
     {
        return !empty(get_option(self::$option_key));
     }

}