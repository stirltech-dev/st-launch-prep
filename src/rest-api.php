<?php 
namespace ST\LaunchPrep\RestAPI;

use ST\LaunchPrep\Settings;

function setup() {
    add_action( 'rest_api_init', function () {
        register_rest_route( 'st/v1', 'launch-prep', array(
            'methods' => 'POST',
            'callback' => __NAMESPACE__ . '\\process',
            // @see https://www.shawnhooper.ca/2017/02/15/wp-rest-secrets-found-reading-core-code/
            'args' => [
                'provider' => [
                    'required' => true,
                    'type' => 'string',
                ],
                'username' => [
                    'required' => true,
                    'type' => 'string',
                    'validate_callback' => function($param) {
                        return !empty($param);
                    }
                ],
                'password' => [
                    'required' => true,
                    'type' => 'string',
                    'validate_callback' => function($param) {
                        return !empty($param);
                    }
                ],
                'login_uri' => [
                    'required' => false,
                    'format' => 'uri',
                    'validate_callback' => function($param) {
                        return !empty($param) && filter_var($param, FILTER_VALIDATE_URL);
                    }
                ]
            ]
        ));
    }); 
}

function process(\WP_REST_Request $request) {
    Settings::save([
        'provider' => $request->get_param('provider'),
        'username' => $request->get_param('username'),
        'password' => $request->get_param('password'),
        'login_uri' => $request->get_param('login_uri'),
    ]);
    send_notice();
    wp_send_json_success();
}

function send_notice() {
    $to = get_option('admin_email');
    $subject = 'New Domain or DNS Credentials Received';
    $body = sprintf(
        'Someone from <a href="%s">%s</a> has submitted their domain or DNS credentials.', 
        get_bloginfo( 'name' ),
        get_site_url()
    );
    wp_mail( $to, $subject, $body );
}
  